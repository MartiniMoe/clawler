extends RigidBody2D

var direction = Vector2(0, 0)
var speed = 2000
var damage = 20
var hit_something = false

func _ready():
	$SFXPlayer._set_playing(true)
	$QueueFree.wait_time = $Particles2D.lifetime
	apply_central_impulse(direction * speed)
	$Particles2D.process_material.direction = Vector3(-direction.x, -direction.y, 0)

func _process(delta):
	get_node('/root/Game/MagicArea').global_position = global_position

func _on_Projectile_body_entered(body):
	if "enemy" in body.get_groups():
		body.health -= damage
		
		if get_node('/root/Game/MagicArea').monitoring:
			for bodi in get_node('/root/Game/MagicArea').get_overlapping_bodies():
				if "enemy" in bodi.get_groups():
					bodi.health -= damage/2

			get_node('/root/Game/MagicArea').monitoring = false		
		elif get_node('/root/Game/Player').equipped_mask == Gamestate.Masks.VAMPIRE:
			get_node('/root/Game/Player').health += damage / 2
			if get_node('/root/Game/Player').health > 100:
				get_node('/root/Game/Player').health = 100	
				
	if not "player" in body.get_groups():
		hit()
		
func hit():
	if not hit_something:
		set_collision_layer_bit(0, false)
		set_collision_mask_bit(0, false)
		set_collision_layer_bit(1, false)
		set_collision_mask_bit(1, false)
		set_linear_velocity(Vector2(0, 0))
		hit_something = true
		$QueueFree.wait_time = $Particles2D.lifetime
		$QueueFree.start()
		$Particles2D.emitting = false
		$Sprite.hide()

func _on_QueueFree_timeout():
	queue_free()
