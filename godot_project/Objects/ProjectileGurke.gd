extends RigidBody2D

var direction = Vector2(0, 0)
var speed = 1500
var damage = 20

func _ready():
	#direction = (Gamestate.player.position - position).normalized()
	rotation_degrees = rand_range(0, 359)
	apply_central_impulse(direction * speed)
	var backwards = randi()%2
	if backwards == 1:
		$AnimationPlayer.play_backwards("rotate")


func _on_ProjectileGurke_body_entered(body):
	if "player" in body.get_groups():
		Gamestate.player.health -= damage
	if not "gatling" in body.get_groups() and not "projectilegurke" in body.get_groups():
		queue_free()
