extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Area2D_body_entered(body):
	if not "player" in body.get_groups(): return
	if Gamestate.player.equipped_mask == Gamestate.Masks.VAMPIRE:
		return
	
	Gamestate.player.input_enabled = false
	Gamestate.player.visible = false
	$Sprite.visible = false
	$Sprite2.visible = true
	
	var t = Timer.new()
	t.set_wait_time(3)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	yield(t, "timeout")
	t.queue_free()
	
	Gamestate.player.input_enabled = true
	Gamestate.player.visible = true
	$Sprite.visible = true
	$Sprite2.visible = false
	Gamestate.player.emit_signal('change_mask_state', Gamestate.Masks.VAMPIRE)
