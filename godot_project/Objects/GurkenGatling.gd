extends Area2D

var rate_of_fire = 0.3
var burst_length = 1
var pause_between_bursts = 2
var firing = false
var enabled = true
var projectile = preload("res://Objects/ProjectileGurke.tscn")
var rotation_speed = 1

func _ready():
	$BurstTimer.wait_time = burst_length
	$PauseTimer.wait_time = pause_between_bursts
	$ShotTimer.wait_time = rate_of_fire
	$PauseTimer.start()
	
func _process(delta):
	var target_rotation = position.angle_to_point(Gamestate.player.position)
	$Gatling.set_rotation(lerp_angle($Gatling.rotation, target_rotation, rotation_speed * delta))


func _on_PauseTimer_timeout():
	# FIRE
	firing = true
	$BurstTimer.start()


func _on_BurstTimer_timeout():
	# STOP FIRING
	firing = false
	$PauseTimer.start()


func _on_ShotTimer_timeout():
	if firing and enabled and Gamestate.player.alive:
		var new_projectile = projectile.instance()
		new_projectile.direction = Vector2(-1, 0).rotated($Gatling.rotation)
		new_projectile.position = position
		get_parent().add_child(new_projectile)


func _on_VisibilityNotifier2D_screen_entered():
	enabled = true


func _on_VisibilityNotifier2D_screen_exited():
	enabled = false
