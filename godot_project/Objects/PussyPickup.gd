extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Area2D_body_entered(body):
	if not "player" in body.get_groups(): return

	# Add mask to inventory
	get_node('/root/Game/Player').emit_signal('change_mask_state', Gamestate.Masks.PUSS)
	
	$Sprite.visible = false
	$Sprite2.visible = false
	$Area2D.set_deferred('monitoring', false)
