extends KinematicBody2D

export var health = 100
export var speed = 3000
var attacking_player = false
export var attack_speed = 1.0
export var damage = 10
var alive = true

func _ready():
	$HealthBar.max_value = health
	$AttackTimer.wait_time = attack_speed

func _process(delta):
	$HealthBar.value = health
	rotation_degrees = 0
	
	if health <= 0 and alive:
		alive = false
		Gamestate.kill_counter += 1
		$AnimationPlayer.play("die")
		set_collision_layer_bit(0, false)
		set_collision_mask_bit(0, false)
		set_collision_layer_bit(1, false)
		set_collision_mask_bit(1, false)
		if 'elon' in get_groups():
			get_node('/root/Game/Level').emit_signal('toggle_room_lock', false)
			Gamestate.isBossLevel = false
	elif Gamestate.player.alive and alive:
		var target_direction = (Gamestate.player.position - position).normalized()
		if target_direction.x > 0:
			$Sprite.flip_h = true
			$Sprite/Tail.flip_h = true
		else:
			$Sprite.flip_h = false
			$Sprite/Tail.flip_h = false
		move_and_slide(target_direction * speed * delta)
	
	attacking_player = false
	for i in get_slide_count():
		var collision = get_slide_collision(i)
		if collision:
			if collision.collider:
				if "player" in collision.collider.get_groups():
					attacking_player = true

func attack():
	if attacking_player:
		if not $AnimationPlayer.is_playing():
			$AnimationPlayer.play("attack")
		Gamestate.player.health -= damage


func _on_AttackTimer_timeout():
	attack()
