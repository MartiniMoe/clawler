extends KinematicBody2D

var speed = 20000
var projectile = preload("res://Objects/Projectile.tscn")
var direction_facing = Vector2(-1, 0)
var health
var health_max = 100
var health_regeneration = 1
var damage = 10
var alive = true
var lifesleft = 9
var input_enabled = true
var movement_vector = Vector2(0, 0)

# 0: keine; 1: magie; 2: puss; 3: vampire
var equipped_mask = 0

signal health_changed
signal health_depleted
signal death
signal change_mask_state(nMask)

# Called when the node enters the scene tree for the first time.
func _ready():
	Gamestate.player = self
	health = health_max
	lifesleft = 9

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	movement_vector = Vector2(0, 0)
	if alive and input_enabled:
		if Input.is_action_pressed("ui_right"):
			movement_vector += Vector2(1, 0)
			$PlayerTextureAnimated.flip_h = true
			FlipOverlay(true)
			$PlayerTextureAnimated.play("walking")
		elif Input.is_action_pressed("ui_left"):
			movement_vector -= Vector2(1, 0)
			$PlayerTextureAnimated.flip_h = false
			FlipOverlay(false)
			$PlayerTextureAnimated.play("walking")
		if Input.is_action_pressed("ui_down"):
			movement_vector += Vector2(0, 1)
			$PlayerTextureAnimated.play("walking")
		elif Input.is_action_pressed("ui_up"):
			movement_vector -= Vector2(0 , 1) 
			$PlayerTextureAnimated.play("walking")
	
		movement_vector = movement_vector.normalized()
	
		if movement_vector != Vector2(0, 0):
			direction_facing = movement_vector
		else:
			$PlayerTextureAnimated.frame = 0
			$PlayerTextureAnimated.stop()

		if Input.is_action_just_pressed("fire"):
			var mousepos = get_viewport().get_mouse_position() + (Gamestate.camera.position - Vector2(960, 550))
			var directionproj = (mousepos - position).normalized()
			
			var new_projectile = projectile.instance()
			new_projectile.direction = directionproj
			new_projectile.position = position + 10 * directionproj
			get_parent().add_child(new_projectile)
		
	if health <= 0 and alive:
		die()
	
	movement_vector *= speed * delta
	move_and_slide(movement_vector)

func _on_AnimationPlayer_animation_finished(anim_name):
	pass # Replace with function body.
	
func die():
	alive = false
	$AnimationPlayer.play("die")
	movement_vector = Vector2.ZERO
	lifesleft -= 1
	switch_to_death_screen()

func switch_to_death_screen():
	if lifesleft == 0:
		get_tree().change_scene("res://Screens/Death_Animation_DoorClosed.tscn")
		
		var t = Timer.new()
		t.set_wait_time(5)
		t.set_one_shot(true)
		self.add_child(t)
		t.start()
		yield(t, "timeout")
		t.queue_free()
	
		get_tree().change_scene("res://Screens/Death_Screen.tscn")
	else:
		get_tree().change_scene("res://Screens/Death_Animation_Door.tscn")

func FlipOverlay(flipped):
	match equipped_mask:
		Gamestate.Masks.MAGIC: $PlayerTextureAnimated/OverlayMagic.flip_h = flipped
		Gamestate.Masks.PUSS: $PlayerTextureAnimated/OverlayPuss.flip_h = flipped
		Gamestate.Masks.VAMPIRE:
			$OverlayVampBack.flip_h = flipped
			$PlayerTextureAnimated/OverlayVampFore.flip_h = flipped

func _on_Player_change_mask_state(nMask):
	var oldMask = equipped_mask
	match oldMask:
		Gamestate.Masks.MAGIC: 
			$PlayerTextureAnimated/OverlayMagic.visible = false
			get_node('/root/Game/MagicArea').monitoring = false
		Gamestate.Masks.PUSS: 
			$PlayerTextureAnimated/OverlayPuss.visible = false
			speed -= 100000/2
		Gamestate.Masks.VAMPIRE:
			$OverlayVampBack.visible = false
			$PlayerTextureAnimated/OverlayVampFore.visible = false
	
	equipped_mask = nMask
	match equipped_mask:
		Gamestate.Masks.MAGIC: 
			$PlayerTextureAnimated/OverlayMagic.visible = true
			get_node('/root/Game/MagicArea').monitoring = true
		Gamestate.Masks.PUSS: 
			$PlayerTextureAnimated/OverlayPuss.visible = true
			speed += 100000/2

		Gamestate.Masks.VAMPIRE:
			$OverlayVampBack.visible = true
			$PlayerTextureAnimated/OverlayVampFore.visible = true
