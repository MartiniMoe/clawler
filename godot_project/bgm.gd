extends Node


var options = ["res://assets/audio/01 - Fight Theme (_) - 29.10.22, 00.43.mp3", "res://assets/audio/02 - Fight Theme 2 . Boss Theme (_) - 29.10.22, 00.35.mp3", "res://assets/audio/04 - Mexikanische Meeresfrüchte - 29.10.22, 03.12.mp3"]
var current_song
var previous_song
var rand_index:int = randi() % options.size()
var random = RandomNumberGenerator.new()
var bossMusicPlaying = false


# Called when the node enters the scene tree for the first time.
func _ready():
#	random.randomize()
	

	if !previous_song:
		current_song = get_song()
		get_node("../BGM").set_stream(load(current_song))
		get_node("../BGM")._set_playing(true)
		
func _process(delta):
		if Gamestate.isBossLevel == true:
			if not bossMusicPlaying:
				get_node("../BGM")._set_playing(false)
				get_node("../BGM").set_stream(load("res://assets/audio/03 - _ - 29.10.22, 02.46.mp3"))
				get_node("../BGM")._set_playing(true)
				bossMusicPlaying = true
		
func get_song():
	var random_song = options[randi() % options.size()]
	while random_song == previous_song:
		random_song = options[randi() % options.size()]
		
	previous_song = random_song
	
	return random_song
	
		
		
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
