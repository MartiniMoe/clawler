extends Camera2D

signal move(modX, modY)

# Called when the node enters the scene tree for the first time.
func _ready():
	Gamestate.camera = self


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Camera2D_move(modX, modY):
	get_node('/root/Game/Camera2D').transform.origin.x += modX
	get_node('/root/Game/Camera2D').transform.origin.y += modY
