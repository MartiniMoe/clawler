extends Control

func _ready():
	Gamestate.ui = self

func _process(delta):
	$HealthProgress.value = Gamestate.player.health
	$TimeContainer/Timer.text = str(int(Gamestate.time_elapsed))
	$KillContainer/Label.text = str(int(Gamestate.kill_counter))
