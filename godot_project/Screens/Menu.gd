extends Control

func _process(delta):
	if Input.is_action_just_pressed("ui_accept"):
		_on_Play_pressed()

func _on_Play_pressed():
	get_tree().change_scene("res://Game.tscn")


func _on_ostlink_meta_clicked(meta):
	OS.shell_open(str(meta))
