extends Node2D

# warning-ignore-all:integer_division

# warning-ignore:unused_signal
signal room_change(body)
signal toggle_room_lock(locked)

var magic = preload("res://Objects/MagicPickup.tscn")
var puss = preload("res://Objects/PussyPickup.tscn")
var sarg = preload("res://Objects/Sarg.tscn")

var magicSpawned = false
var pussySpawned = false
var coffinSpawned = false

var rat = preload("res://Characters/Enemy.tscn")
var scoutfromtf2 = preload("res://Characters/Enemy_small.tscn")
var elonmask = preload("res://Characters/Boss.tscn")
var gurkengattling = preload("res://Objects/GurkenGatling.tscn")

var muskstance = null

const ROOM_WIDTH = 30
const ROOM_HEIGHT = 17
var   WORLD_ROOM_WIDTH = 0
var   WORLD_ROOM_HEIGHT = 0
const DOOR_SIZE = 3
var   WORLD_TILE_SIZE = 0

# [ [Top X, Top Y, Entry Direction], ... ]
var UNGENERATED_ROOMS = []

# [ Room: [Top X, Top Y, Doors: [ [x, y], ... ] ]
var ROOMS = []
var CURRENT_ROOM = 0
var CATNIP_VALUE = 0.0000000000

# Called when the node enters the scene tree for the first time.
enum Direction { NONE, NORTH, SOUTH, EAST, WEST }

var rng = RandomNumberGenerator.new()
func _ready():
	Gamestate.current_level = self
	WORLD_ROOM_WIDTH = ROOM_WIDTH * $TileMap.to_global($TileMap.map_to_world(Vector2(1, 1), false)).x
	WORLD_ROOM_HEIGHT = ROOM_HEIGHT * $TileMap.to_global($TileMap.map_to_world(Vector2(1, 1), false)).y
	WORLD_TILE_SIZE = $TileMap.to_global($TileMap.map_to_world(Vector2(1, 1), false)).y
	rng.randomize()
	CATNIP_VALUE = rng.randf_range(0,0.33)
	GenRoom(0, 0, Direction.NORTH)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func OppositeCardinal(dir):
	match dir:
		Direction.NORTH: return Direction.SOUTH
		Direction.SOUTH: return Direction.NORTH
		Direction.WEST: return Direction.EAST
		Direction.EAST: return Direction.WEST

func IncInDir(direction, val, amount):
	match direction:
		Direction.NORTH, Direction.WEST: return val-amount
		Direction.SOUTH, Direction.EAST: return val+amount
		
	return val
	
func RoomCenter():
	var minX = ROOMS[CURRENT_ROOM][0].x
	var minY = ROOMS[CURRENT_ROOM][0].y
	
	return Vector2(minX + ( WORLD_ROOM_WIDTH / 2 ) , minY + ( WORLD_ROOM_HEIGHT / 2 ))
	
func RandomPositionInRoom():
	var minX = ROOMS[CURRENT_ROOM][0].x+ROOMS[CURRENT_ROOM][0].x / WORLD_ROOM_WIDTH
	var minY = ROOMS[CURRENT_ROOM][0].y+ROOMS[CURRENT_ROOM][0].y / WORLD_ROOM_HEIGHT
	var maxX = ROOMS[CURRENT_ROOM][1].x-ROOMS[CURRENT_ROOM][0].x / WORLD_ROOM_WIDTH
	var maxY = ROOMS[CURRENT_ROOM][1].y-ROOMS[CURRENT_ROOM][0].y / WORLD_ROOM_HEIGHT
	
	return Vector2(rng.randi_range(minX, maxX), rng.randi_range(minY, maxY))

func GenRoom(tmX, tmY, entryDirection):
	if (CATNIP_VALUE >= 1):
		CATNIP_VALUE = rng.randf_range(0.1, 0.33)
	
	# Walls
	# North + South
	print('genroom')
	for i in range(0, ROOM_WIDTH):
		$TileMap.set_cell(tmX+i, tmY, 14)
		$TileMap.set_cell(tmX+i, tmY+ROOM_HEIGHT-1, 14)

	if IsWallDoor(tmX, tmY, Direction.SOUTH): # If opposite is door, make door
		PlaceDoor(tmX, tmY, Direction.SOUTH)
		print ('South is door')
	if IsWallDoor(tmX, tmY, Direction.NORTH): # If opposite is door, make door
		PlaceDoor(tmX, tmY, Direction.NORTH)
		print ('North is door')

	# West + East
	for i in range(0, ROOM_HEIGHT):
		$TileMap.set_cell(tmX, tmY+i, 14)
		$TileMap.set_cell(tmX+ROOM_WIDTH-1, tmY+i, 14)

	if IsWallDoor(tmX, tmY, Direction.WEST): # If opposite is door, make door
		PlaceDoor(tmX, tmY, Direction.WEST)
		print ('West is door')
	if IsWallDoor(tmX, tmY, Direction.EAST): # If opposite side door, make door
		PlaceDoor(tmX, tmY, Direction.EAST)
		print ('East is door')

	for x in range(tmX+1, tmX+ROOM_WIDTH-1):
		for y in range(tmY+1, tmY+ROOM_HEIGHT-1):
			$TileMap.set_cell(x, y, 0)
			if rng.randi_range(0, 200) > 100 and rng.randi_range(0, 200) < 15:
				$TileMap.set_cell(x, y, 12)

	CATNIP_VALUE += rng.randf_range(0.1, 0.2)

	# Room type should be decided here

	# Doors
	randomize()
	var doors = []
	for i in range(1, 5):
		if (i == entryDirection) or (randi() % 2):
			if not DoorPossible(tmX, tmY, i): continue
			PlaceDoor(tmX, tmY, i)

			var stX = tmX
			var stY = tmY
			if i == Direction.NORTH or i == Direction.SOUTH:
				stY =  IncInDir(i, tmY, ROOM_HEIGHT)
			else:
				stX =  IncInDir(i, tmX, ROOM_WIDTH)
				
			doors += [i]

			UNGENERATED_ROOMS += [[stX, stY, OppositeCardinal(i)]]

	ROOMS += [
				[
					$TileMap.to_global($TileMap.map_to_world(Vector2(tmX, tmY), false)), 
					$TileMap.to_global($TileMap.map_to_world(Vector2(tmX+ROOM_WIDTH, tmY+ROOM_HEIGHT), false)),
					Vector2(tmX, tmY), doors
				]
			] 

	# Random Spawns and Shit
	if CATNIP_VALUE > 0.20 and CATNIP_VALUE < 0.5:
		for i in range(0, rng.randi_range(1, 6)):
			var enemy = rat.instance()
			enemy.position = RandomPositionInRoom()
			enemy.visible = true
			get_parent().call_deferred('add_child', enemy)
			
		CATNIP_VALUE -= 0.05

	if CATNIP_VALUE > 0.46 and CATNIP_VALUE < 0.7:
		for i in range(0, rng.randi_range(1, 3)):
			var enemy = scoutfromtf2.instance()
			enemy.position = RandomPositionInRoom()
			enemy.visible = true
			get_parent().call_deferred('add_child', enemy)
			
		CATNIP_VALUE -= 0.02

	var sashaWasTouched = false
	if CATNIP_VALUE > 0.32168 and CATNIP_VALUE < 0.4:
		CATNIP_VALUE += 0.1805

		var sasha = gurkengattling.instance()
		sasha.position = RoomCenter()
		sasha.visible = true
		get_parent().call_deferred('add_child', sasha)
		sashaWasTouched = true

	print (CATNIP_VALUE)
	if CATNIP_VALUE > 0.4 and CATNIP_VALUE < 0.8 and not sashaWasTouched:
		if not magicSpawned or not pussySpawned or not coffinSpawned:
			var mag = null
			while true:
				var spawn = rng.randi_range(0, 3)
				if spawn == 0 and not magicSpawned:
					mag = magic.instance()
					magicSpawned = true
					break
				elif spawn == 1 and not pussySpawned:
					mag = puss.instance()
					pussySpawned = true
					break
				elif spawn == 2 and not coffinSpawned:
					mag = sarg.instance()
					coffinSpawned = true
					break

			mag.position = RoomCenter()
			get_parent().call_deferred('add_child', mag)
			CATNIP_VALUE += 0.1337
	elif CATNIP_VALUE > 0.8:
		if rng.randi_range(0, 10000) > 7000 and rng.randi_range(0, 1000) < 9000:
			muskstance = elonmask.instance()
			muskstance.position = RoomCenter()
			
			get_parent().call_deferred('add_child', muskstance)
			CATNIP_VALUE = 0.01
			Gamestate.isBossLevel = true
			emit_signal('toggle_room_lock', true)
			get_node('/root/Game/Player').position = Vector2(
				IncInDir(OppositeCardinal(entryDirection), get_node('/root/Game/Player').position.x, WORLD_TILE_SIZE),
				IncInDir(OppositeCardinal(entryDirection), get_node('/root/Game/Player').position.y, WORLD_TILE_SIZE)
			)
		else:
			CATNIP_VALUE -= 0.5

# Checks if the given wall has a door
func IsWallDoor(oX, oY, dir):
	var x = 0
	var y = 0
	if dir == Direction.SOUTH:
		y = oY+ROOM_HEIGHT+1
	elif dir == Direction.EAST:
		x = oX+ROOM_WIDTH+1
	elif dir == Direction.NORTH:
		y = oY-ROOM_HEIGHT-1
	elif dir == Direction.WEST:
		x = oX-ROOM_WIDTH-1

	var room = -1
	for i in range(0, len(ROOMS)):
		if ROOMS[i][2].x == x and ROOMS[i][2].y == y:
			room = i

	if room == -1: return false
	return dir in ROOMS[room][3]

# Checks if its possible for a door on the given wall to be passed through
func DoorPossible(oX, oY, direction):
	var x = oX-1
	var y = oY-1
	if direction == Direction.SOUTH:
		y = oY+ROOM_HEIGHT+1
	elif direction == Direction.EAST:
		x = oX+ROOM_WIDTH+1

	if direction == Direction.NORTH or direction == Direction.SOUTH:
		x = oX + int( (ROOM_WIDTH / 2) - (DOOR_SIZE / 2) )
	else:
		y = oY + int( (ROOM_HEIGHT / 2) - (DOOR_SIZE / 2) )

	if ($TileMap.get_cellv(Vector2(x, y)) == 14):
		return false

	return true

func PlaceDoor(oX, oY, direction, locked = false):
	var x = oX
	var y = oY
	if direction == Direction.SOUTH:
		y = oY+ROOM_HEIGHT-1
	elif direction == Direction.EAST:
		x = oX+ROOM_WIDTH-1

	for i in range(0, 3):
		if direction == Direction.NORTH or direction == Direction.SOUTH:
			x = oX + int( (ROOM_WIDTH / 2) - (DOOR_SIZE / 2) ) + i
		else:
			y = oY + int( (ROOM_HEIGHT / 2) - (DOOR_SIZE / 2) ) + i
			
		if not locked:
			$TileMap.set_cell(x, y, 0)
		else:
			$TileMap.set_cell(x, y, 19)

func SetOrGenRoom(x, y):
	var gpos = $TileMap.to_global($TileMap.map_to_world(Vector2(x,y), false))
	for i in range(0, len(ROOMS)):
		if ROOMS[i][0].x == gpos.x and ROOMS[i][0].y == gpos.y:
			CURRENT_ROOM = i
			return

	for i in range(0, len(UNGENERATED_ROOMS)):
		if UNGENERATED_ROOMS[i][0] == x and UNGENERATED_ROOMS[i][1] == y:
			CURRENT_ROOM = len(ROOMS)
			GenRoom(x, y, UNGENERATED_ROOMS[i][2])
			UNGENERATED_ROOMS.remove(i)

func _on_Level_room_change(body): 
	if not "player" in body.get_groups(): return

	var modX = 0
	var modTX = 0
	var modY = 0
	var modTY = 0

	if (body.position.x < ROOMS[CURRENT_ROOM][0].x):
		modX = -WORLD_ROOM_WIDTH
		modTX = -ROOM_WIDTH
	elif (body.position.x > ROOMS[CURRENT_ROOM][1].x):
		modX = WORLD_ROOM_WIDTH
		modTX = ROOM_WIDTH
	elif (body.position.y < ROOMS[CURRENT_ROOM][0].y):
		modY = -WORLD_ROOM_HEIGHT
		modTY = -ROOM_HEIGHT
	elif (body.position.y > ROOMS[CURRENT_ROOM][1].y):
		modY = WORLD_ROOM_HEIGHT
		modTY = ROOM_HEIGHT

	Gamestate.isBossLevel = false
	SetOrGenRoom(ROOMS[CURRENT_ROOM][2].x+modTX, ROOMS[CURRENT_ROOM][2].y+modTY)
	get_node('/root/Game/Camera2D').emit_signal('move', modX, modY)
	$Area2D.transform.origin.x += modX
	$Area2D.transform.origin.y += modY


func _on_Level_toggle_room_lock(locked):
	var tmX = ROOMS[CURRENT_ROOM][2].x
	var tmY = ROOMS[CURRENT_ROOM][2].y
	for door in ROOMS[CURRENT_ROOM][3]:
		PlaceDoor(tmX, tmY, door, locked)
