# Clawler
Ein dunkler, flauschiger Dungeon Crawler. <br>
Entstanden im Rahmen des [HaSi](https://ha.si) Gamejams 2022.
## Name:
Der Name ergibt sich aus Claw (wegen Katze :3) und Crawler (wegen Dungeon Crawler).

---

## Story
Du findest dich in einem dunklen, modrigen Raum wieder. Das letzte an das du dich erinnern kannst ist wie du dir den Kopf in deiner Kueche gestossen hast und ohnmaechtig wurdest. Du schaust an dir herunter und siehst Fell. In einer Pfuetze betrachtest du dich. Warte wie kannst du ueberhaupt was sehen? Es ist stockdunkel. Dann faellt dir auf, dass dein Gesicht anders aussieht als sonst und du begreifst: DU BIST EINE KATZE!

---

![text](./screenshot.png)

---

# SCRAP AFTER RELEASE:
# BraIInstorming Nummer 2 oder so

## Mechanik
- Godot
- Dynamische/Statische Level
	- randomized Raum-Presets
- Element-Masken (Feuer, Wasser, etc.)/(Magie-Hut?)
- Projektil System/Nahkampfmaske
-  Masken in Truhen finden
- ((Auf Klima angepasste Räumer))

## Setting
- Dunkel
- Unterirdisch
- Gemäuer
- Pixel-Art

## Audio
- Crossfade/Looping


